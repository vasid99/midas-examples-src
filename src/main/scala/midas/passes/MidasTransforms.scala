// See LICENSE for license details.

package midas
package passes

import midas.core._
import firrtl._
import firrtl.ir._
import firrtl.Mappers._
import firrtl.annotations._
import Utils._
import java.io.{File, FileWriter}

private class WCircuit(
  // (??) someone please help with WIR.scala, idk what's going on over there
  info: Info,
  modules: Seq[DefModule],
  main: String,
  val sim: SimWrapperIO) extends Circuit(info, modules, main)

object MidasAnnotation {
  // appropriate annotation
  def apply(t: String, conf: File, json: File, lib: Option[File]) =
    Annotation(CircuitName(t), classOf[MidasTransforms],
               s"$conf $json %s".format(lib map (_.toString) getOrElse ""))
  private val matcher = "([^ ]+) ([^ ]+) ([^ ]*)".r
  def unapply(a: Annotation) = a match {
    case Annotation(CircuitName(c), t, matcher(conf, json, lib))
      if t == classOf[MidasTransforms] =>
        Some(c, new File(conf), new File(json), if (lib.isEmpty) None else Some(new File(lib)))
    case _ => None
  }
}

private[midas] class MidasTransforms(
    dir: File,
    io: chisel3.Data)
   (implicit param: config.Parameters) extends Transform {
  // input (target) LoFIRRTL to output (decoupled host) LoFIRRTL
  def inputForm = LowForm
  def outputForm = LowForm
  // DO IT, LIEUTENANT
  def execute(state: CircuitState) = (getMyAnnotations(state): @unchecked) match {
    case Seq(MidasAnnotation(state.circuit.main, conf, json, lib)) =>
      // make a list (or rather, sequence) of all the transforms
      val xforms = Seq(
        // standard FIRRTL passes
        firrtl.passes.RemoveValidIf,
        new firrtl.transforms.ConstantPropagation,
        firrtl.passes.SplitExpressions,
        firrtl.passes.CommonSubexpressionElimination,
        new firrtl.transforms.DeadCodeElimination,
        // (?) related to annotation
        new ConfToJSON(conf, json),
        // (??) belonging to barstools package
        new barstools.macros.MacroCompilerTransform,
        // more standard FIRRTL passes
        firrtl.passes.ResolveKinds,
        firrtl.passes.RemoveEmpty,
        // there! there! he said it!
        new Fame1Transform(lib getOrElse json),
        // strober stuff
        new strober.passes.StroberTransforms(dir, lib getOrElse json),
        // tbd
        new SimulationMapping(io),
        new PlatformMapping(state.circuit.main, dir))
      // (??) foldLeft? I am seeing it everywhere but don't know what it is
      (xforms foldLeft state)((in, xform) =>
        xform runTransform in).copy(form=outputForm)
  }
}
