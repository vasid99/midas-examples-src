* **core** - implementations of decoupled interfaces and token channels for host, as well as the vanilla surrounding it (endpoints, FPGA wrappers, sim wrappers)
    - *Channel.scala* - declarations for token channels and their IO
    - *Endpoints.scala* - declarations for host sim endpoints, such as memory
    - *FPGATop.scala* - (verbatim) Platform agnostic wrapper of the simulation models for FPGA 
    - *Interfaces.scala* - declarations for IO interfacing ports on hist design
    - *SimWrapper.scala* - (maybe?) full wrapper that takes target module to decoupled host module
* **passes** - 
    - *Fame1Transform.scala* - creates targetFire and daisyReset ports, collects all enable ports, connects everything appropriately and runs the whole sequence mentioned above
    - *MidasTransforms.scala* - runs FAME1 transform in tandem with many other transforms, including platform and simulation mapping
    - *PlatformMapping.scala* - C header file generator for FPGA-platform-specific things
    - *SimulationMapping.scala* - creates the circuit for the given simulation wrapper
    - *Utils.scala* - general utils
* **platform** - specific to testing platform
    - *ZynqShim.scala* - platform library for Zynq FPGAs
* **widgets** - some specific IPs used in the transform
* *Compiler.scala* - incorporates everything else (by using MidasTransforms) and emits FIRRTL/Verilog
* *Config.scala* - all the options ised in the above files (ChannelLin, Ctrl/MemNastiKey, EnableSnapshot,...)
