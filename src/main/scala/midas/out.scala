--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Compiler.scala
private class MidasCompiler(dir: File, io: Data)(implicit param: config.Parameters) extends firrtl.Compiler {
private class VerilogCompiler extends firrtl.Compiler {
object MidasCompiler {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Config.scala
case object Zynq extends PlatformType
case object Platform extends Field[PlatformType]
case object EnableSnapshot extends Field[Boolean]
case object KeepSamplesInMem extends Field[Boolean]
case object MemModelKey extends Field[Option[Parameters => MemModel]]
case object EndpointKey extends Field[EndpointMap]
class SimConfig extends Config((site, here, up) => {
class ZynqConfig extends Config(new Config((site, here, up) => {
class ZynqConfigWithSnapshot extends Config(new Config((site, here, up) => {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HTMLnotes.html

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
notes.md

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
out.scala

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
core/Channel.scala
class WireChannelIO(w: Int)(implicit p: Parameters) extends Bundle {
class WireChannel(val w: Int)(implicit p: Parameters) extends Module {
class SimReadyValidIO[T <: Data](gen: T) extends Bundle {
object SimReadyValid {
class ReadyValidTraceIO[T <: Data](gen: T) extends Bundle {
object ReadyValidTrace {
class ReadyValidChannelIO[T <: Data](gen: T)(implicit p: Parameters) extends Bundle {
class ReadyValidChannel[T <: Data](gen: T, flipped: Boolean, n: Int = 2)(implicit p: Parameters) extends Module {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
core/Endpoints.scala
abstract class SimMemIO extends Endpoint {
class SimNastiMemIO extends SimMemIO {
class SimAXI4MemIO extends SimMemIO {
case class EndpointMap(endpoints: Seq[Endpoint]) {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
core/FPGATop.scala
case object MemNastiKey extends Field[NastiParameters]
case object FpgaMMIOSize extends Field[BigInt]
class FPGATopIO(implicit p: Parameters) extends _root_.util.ParameterizedBundle()(p) {
class FPGATop(simIoType: SimWrapperIO)(implicit p: Parameters) extends Module with HasWidgets {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
core/Interfaces.scala
class HostDecoupledIO[+T <: Data](gen: T) extends Bundle
object HostDecoupled {
class HostReadyValid extends Bundle {
class HostPortIO[+T <: Data](gen: T, tokenFlip: Boolean) extends Bundle
object HostPort {
class MidasDecoupledIO[+T <: Data](gen: T) extends Bundle
object MidasDecoupled {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
core/SimWrapper.scala
object SimUtils {
case object ChannelLen extends Field[Int]
case object ChannelWidth extends Field[Int]
class SimReadyValidRecord(es: Seq[(String, ReadyValidIO[Data])]) extends Record {
class ReadyValidTraceRecord(es: Seq[(String, ReadyValidIO[Data])]) extends Record {
class SimWrapperIO(
class TargetBox(targetIo: Data) extends BlackBox {
class SimBox(simIo: SimWrapperIO)
class SimWrapper(targetIo: Data)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
passes/Fame1Transform.scala
private[passes] class Fame1Transform(json: java.io.File) extends firrtl.passes.Pass {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
passes/MidasTransforms.scala
private class WCircuit(
object MidasAnnotation {
Annotation(CircuitName(t), classOf[MidasTransforms],
if t == classOf[MidasTransforms] =>
private[midas] class MidasTransforms(

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
passes/PlatformMapping.scala
private[passes] class PlatformMapping(

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
passes/SimulationMapping.scala
private[passes] class SimulationMapping(

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
passes/Utils.scala
object Utils {
case class MemConf(
object MemConfReader {
case object Name extends ConfField
case object Depth extends ConfField
case object Width extends ConfField
case object Ports extends ConfField
case object MaskGran extends ConfField
class ConfToJSON(conf: File, json: File) extends Transform {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
platform/ZynqShim.scala
abstract class PlatformShim(implicit p: Parameters) extends Module {
case object MasterNastiKey extends Field[NastiParameters]
case object SlaveNastiKey extends Field[NastiParameters]
class ZynqShimIO(implicit p: Parameters) extends ParameterizedBundle()(p) {
class ZynqShim(simIo: midas.core.SimWrapperIO)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
widgets/CppGeneration.scala
case class UInt32(value: BigInt) extends IntLikeLiteral {
case class UInt64(value: BigInt) extends IntLikeLiteral {
case class CStrLit(val value: String) extends CPPLiteral {
object CppGenerationUtils {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
widgets/Lib.scala
object FlattenData {
object ScanRegister {
class SatUpDownCounterIO(val n: Int) extends Bundle {
class SatUpDownCounter(val n: Int) extends Module {
object SatUpDownCounter {
class MultiQueueIO[T <: Data](gen: T, numQueues: Int, entries: Int) extends
class MultiQueue[T <: Data](
object HostMux {
case class Permissions(readable: Boolean, writeable: Boolean)
object ReadOnly extends Permissions(true, false)
object WriteOnly extends Permissions(false, true)
object ReadWrite extends Permissions(true, true)
abstract class MCRMapEntry {
case class DecoupledSinkEntry(node: DecoupledIO[UInt], name: String) extends MCRMapEntry {
case class DecoupledSourceEntry(node: DecoupledIO[UInt], name: String) extends MCRMapEntry {
case class RegisterEntry(node: Data, name: String, permissions: Permissions) extends MCRMapEntry
class MCRFileMap() {
class MCRIO(numCRs: Int)(implicit p: Parameters) extends NastiBundle()(p) {
class MCRFile(numRegs: Int)(implicit p: Parameters) extends NastiModule()(p) {
class CRIO(val direction: Direction, width: Int, val default: Int) extends Bundle {
object CRIO {
class DecoupledCRIO[+T <: Data](gen: T) extends DecoupledIO[T](gen) {
object DecoupledCRIO {
object D2V {
object V2D {
object HoldingRegister {
object SkidRegister {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
widgets/LoadMem.scala
class LoadMemIO(hKey: Field[NastiParameters])(implicit p: Parameters) extends WidgetIO()(p){
class NastiParams()(implicit val p: Parameters) extends HasNastiParameters
class LoadMemWidget(hKey: Field[NastiParameters])(implicit p: Parameters) extends Widget()(p) {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
widgets/Master.scala
class EmulationMasterIO(implicit p: Parameters) extends WidgetIO()(p){
object Pulsify {
class EmulationMaster(implicit p: Parameters) extends Widget()(p) {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
widgets/NastiIO.scala
abstract class EndpointWidgetIO(implicit p: Parameters) extends WidgetIO()(p) {
abstract class EndpointWidget(implicit p: Parameters) extends Widget()(p) {
abstract class MemModelConfig // TODO: delete it
class MemModelIO(implicit p: Parameters) extends EndpointWidgetIO()(p){
abstract class MemModel(implicit p: Parameters) extends EndpointWidget()(p){
abstract class NastiWidgetBase(implicit p: Parameters) extends MemModel {
class NastiWidget(implicit val p: Parameters) extends NastiWidgetBase {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
widgets/PeekPokeIO.scala
class PeekPokeIOWidgetIO(inNum: Int, outNum: Int)(implicit p: Parameters)
class PeekPokeIOWidget(inputs: Seq[(String, Int)], outputs: Seq[(String, Int)])

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
widgets/SimpleLatencyPipe.scala
case class MidasLLCParameters(nWays: Int, nSets: Int, blockBytes: Int)
case object MidasLLCKey extends Field[Option[MidasLLCParameters]]
class MidasLLCConfigBundle(key: MidasLLCParameters) extends Bundle {
class MidasLLC(key: MidasLLCParameters)(implicit p: Parameters) extends NastiModule {
class SimpleLatencyPipe(implicit val p: Parameters) extends NastiWidgetBase {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
widgets/Widget.scala
case object CtrlNastiKey extends Field[NastiParameters]
class WidgetMMIO(implicit p: Parameters) extends NastiIO()(p)
object WidgetMMIO {
abstract class WidgetIO(implicit p: Parameters) extends ParameterizedBundle()(p){
abstract class Widget(implicit p: Parameters) extends Module {
wName.getOrElse(throw new  RuntimeException("Must build widgets with their companion object"))
require(_finalized, "Must build Widgets with their companion object")
require(_finalized, "Must build Widgets with their companion object")
object Widget {
object WidgetRegion {

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
